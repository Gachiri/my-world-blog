<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    //

    public function scopeActive($query){
        return $query->where('status', 'active');
    }
}
