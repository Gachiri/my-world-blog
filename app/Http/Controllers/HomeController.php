<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Rate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function posts()
  {
   $posts = Post::all();
   return view('posts',compact('posts'));
  }

 public function show($id)
  {
   $post = Post::find($id);
   return view('postsShow',compact('post'));
  }

 public function postPost(Request $request)
  {
   request()->validate(['rate' => 'required']);
//    $post = Post::find($request->id);
   // $post = Post::where('id', $request->id);
//    $rating = new \willvincent\Rateable\Rating;
   // $rating = new Rate();
   // $rating->rating = $request->rate;
   // $rating->user_id = auth()->user()->id;
//    $rating->rateable_type = 'Post' ;
   // $rating->post_id = $request->id;
   // $rating->save();
   //$post->ratings()->save($rating);

   Rate::create([
      'rating' => $request->rate,
      'user_id' => auth()->user()->id,
      'post_id' => $request->id,

   ]);
   return redirect()->route("posts");
  }
}
