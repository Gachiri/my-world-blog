<?php
namespace App\Http\Controllers;
use App\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class SearchController extends Controller
{
    public function index()
    {
        return view('search.search');
    }
    public function search(Request $request)
    {
        if($request->ajax())
        {
            $output="";
//            $products=DB::table('listings')->where('title','LIKE','%'.$request->search."%")->get();

            $products = Listing::active()->where('title','LIKE','%'.$request->search."%")->get();
            if($products)
            {
                foreach ($products as $key => $product) {
                    $output.='<tr>'.
                        '<td>'.$product->id.'</td>'.
                        '<td>'.$product->Title.'</td>'.
                        '<td>'.$product->Description.'</td>'.
                        '<td>'.$product->status.'</td>'.
                        '</tr>';
                }
                return Response($output);
            }
        }
    }
}
