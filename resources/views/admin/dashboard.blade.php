@extends('layouts.admin')

@section('title', 'Dashboard')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-2 mt-1">
                <div class="card">

                    <div class="card-body bg-white shadow-lg">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('Total Customers') }}
                    </div>
                </div>
            </div>
            <div class="col-md-2 mt-1">
                <div class="card">
                    <div class="card-body bg-white shadow-lg">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('Total Listings') }}
                    </div>
                </div>
            </div>
            <div class="col-md-2 mt-1">
                <div class="card">
                    <div class="card-body bg-white shadow-lg">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('Customers') }}
                    </div>
                </div>
            </div>
            <div class="col-md-2 mt-1">
                <div class="card">
                    <div class="card-body bg-white shadow-lg">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('Live Adverts') }}
                    </div>
                </div>
            </div>
            <div class="col-md-2 mt-1">
                <div class="card">
                    <div class="card-body bg-white shadow-lg">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('Total Listings') }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mt-2">
            <div class="card col-md-10">
                <div class="card-body bg-white shadow-lg">
                    <div class="col">

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('Total Listings') }}
                    </div>
                    <div class="col">

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('Total Listings') }}
                    </div>


                </div>
            </div>
            </div>
        </div>
    </div>

@endsection
