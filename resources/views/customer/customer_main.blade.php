@extends('layouts.app')

@section('content')

    <div class="grid-cust">
        <div class="grid-item item-results">@include('layouts.business-summary')
            @include('layouts.business-summary')
            @include('layouts.business-summary')
            @include('layouts.business-summary')
        </div>
        <div class="grid-item item-cat">Categories</div>
        <div class="grid-item item-ad">@include('layouts.adverts')</div>
        <div class="grid-item item-ad2">More Adverts</div>
        <div class="grid-item item-related"> Related </div>
        <section id="projects" class="projects-section bg-light grid-item item-feedback">
                <div class="container">

                    <!-- Project Two Row -->
                    <div class="row justify-content-center no-gutters">
                        
                        <div class="col-lg-6">
                            <img class="img-fluid" src="{{ asset('images/lion.jpg') }}" alt="">
                        </div>
                        <div class="col-lg-6 order-lg-first">
                            <div class="bg-black text-center h-100 project">
                                <div class="d-flex h-100">
                                    <div class="project-text w-100 my-auto text-center text-lg-right">
                                        <h4 class="text-white">Mountains</h4>
                                        <p class="mb-0 text-white-50">Another example of a project with its respective description. These sections work well responsively as well, try this theme on a small screen!</p>
                                        <hr class="d-none d-lg-block mb-0 mr-0">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
    </div>




@endsection
