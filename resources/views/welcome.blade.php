<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    {{--        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175312296-1"></script>--}}
    {{--        <script>--}}
    {{--            window.dataLayer = window.dataLayer || [];--}}
    {{--            function gtag(){dataLayer.push(arguments);}--}}
    {{--            gtag('js', new Date());--}}

    {{--            gtag('config', 'UA-175312296-1');--}}
    {{--        </script>--}}


    <title>Lowkey</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">




    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <!-- Styles -->
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/grayscale.css') }}" rel="stylesheet">

</head>
<body class="d-flex flex-column">
<div id="page-container">

    @include('layouts.landing-nav')

    <div id="content-wrap">
        <header class="masthead">
            <div class="container d-flex h-100">
                <div class="mx-auto my-5">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <img class="img-fluid" src="{{ asset('images/banner.png') }}" alt="">
                        </div>
                    </div>

{{--                    <h1 class="text-uppercase">lowkey Digitals</h1>--}}
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 mx-auto text-center">
                                <form class="form-inline input-group d-flex shadow-lg">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-dropdown dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                        <div class="dropdown-menu">
                                            <p class="dropdown-item" style="color: #00458B;">Categories</p>
                                            <div role="separator" class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                            <a class="dropdown-item" href="#">Separated link</a>
                                        </div>
                                    </div>
                                        <input type="search" class="form-control"  id="inputEmail" placeholder="Find a Business" aria-label="Find a Business" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-custom" type="submit">Search</button>
                                        </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    {{--                        <h2 class="text-white-50 mx-auto mt-2 mb-5">Where you find a service and grow your business.</h2>--}}

                </div>
            </div>
        </header>

        <section id="projects" class="projects-section bg-light">
            <div class="container">

                <!-- Customer stuff Row -->
                <div class="row align-items-center no-gutters mb-5">
                    <div class="col-lg-6">
                        <div class="featured-text text-center text-lg-left">
                            <h4><i class="fa fa-search"></i> Find a Business  </h4>
                            <p class="text-black-50 mb-0">Looking for a product or a service near you? <br>
                                Search for a business <a href="explore">here</a> and get their contacts and addresses.</p>

                        </div>
                    </div>
                    <div class="col-lg-6 d-flex justify-content-center">
                        <img class="img-fluid main-page-image" src="{{ asset('images/maps.png') }}" alt="">
                    </div>
                </div>

                <hr class="mb-5 mt-5">

                <div class="row align-items-center mb-5 no-gutters">
                    <div class="col-lg-6 d-flex justify-content-center">
                        <img class="img-fluid main-page-image" src="{{ asset('images/banner.png') }}" alt="">
                    </div>
                    <div class="col-lg-6">
                        <div class="featured-text text-center text-lg-left">
                            <h4>Rate a Business ★★★★★</h4>
                            <p class="text-black-50 mb-0">Rate a business, <a href="explore">give a review</a> of their products & services and even get feedback from the business.<br>
                                Check out images and reviews from other customers that will help you pick the best place to get your products & services.</p>
                        </div>
                    </div>
                </div>

                <hr class="mb-5 mt-5">

                <!-- Project One Row -->
                <div class="row justify-content-center no-gutters">
                    <div class="col-lg-5">
                        <img class="img-fluid" src="{{ asset('images/business1.png') }}" alt="">
                    </div>
                    <div class="col-lg-7">
                        <div class="bg-dark text-center h-100 project">
                            <div class="d-flex h-100">
                                <div class="project-text w-100 my-auto text-center text-lg-left">
                                    <h4 class="text-white">Reach Your Customers</h4>
                                    <p class="mb-0 text-white-50">Can your customers find you online? <br> Add a <a href="new_listing">free business listing</a>
                                    and enhance your online presence by managing your own business profile.
                                    Keep your images, contact information and addresses up to date</p>
                                    <hr class="d-none d-lg-block mb-0 ml-0">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Project Two Row -->
                <div class="row justify-content-center no-gutters">
                    <div class="col-lg-5">
                        <img class="img-fluid" src="{{ asset('images/maps.png') }}" alt="">
                    </div>
                    <div class="col-lg-7 order-lg-first">
                        <div class="bg-dark text-center h-100 project">
                            <div class="d-flex h-100">
                                <div class="project-text w-100 my-auto text-center text-lg-right">
                                    <h4 class="text-white">Grow your Business</h4>
                                    <p class="mb-0 text-white-50">Get reviews and ratings from customers that will help you to better package your products and services.<br>
                                        Make your customers aware of your latest offers and events by <a href="advertise">advertising with us</a><br>
                                    Data that will help you keep them coming.</p>
                                    <hr class="d-none d-lg-block mb-0 mr-0">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>


    </div>
    <footer id="footer" class="text-light">
        @include('layouts.footer')
    </footer>
</div>

<script type="text/javascript">
    var topofDiv = $(".masthead").offset().top;
    var height = $(".masthead").outerHeight();

    $(window).scroll(function(){
        if($(window).scrollTop() > (topofDiv + height)){
            $('.main-nav').addClass('navbar-shrink');
        }
        else{
            $('.main-nav').removeClass('navbar-shrink');
        }
    });
</script>

</body>
</html>
