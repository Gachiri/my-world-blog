<!-- Footer -->

    <!-- Footer Links -->
    <div class="container text-center text-md-left">

        <!-- Footer links -->
        <div class="row text-center text-md-left mt-3 pb-3">

            <!-- Grid column -->
            <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                <h6 class="text-uppercase mb-4">Lowkey Digital</h6>
                <img src="{{ asset('images/small-logo.png') }}" class="img-fluid small-logo">

            </div>
            <!-- Grid column -->

            <hr class="w-100 clearfix d-md-none">

            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                <h6 class="text-uppercase mb-4">Products</h6>
                <p>
                    <a href="#!">Give a Review</a>
                </p>
                <p>
                    <a href="#!">Add a Business Listing</a>
                </p>
                <p>
                    <a href="#!">Claim A Business Listing</a>
                </p>
                <p>
                    <a href="#!">Advertise With Us</a>
                </p>
            </div>
            <!-- Grid column -->

            <hr class="w-100 clearfix d-md-none">

            <!-- Grid column -->
            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                <h6 class="text-uppercase mb-4">Useful links</h6>
                <p>
                    <a href="#!">Explore Businesses</a>
                </p>
                <p>
                    <a href="#!">Customer Profile</a>
                </p>
                <p>
                    <a href="#!">Manage Business Account</a>
                </p>
                <p>
                    <a href="#!">Give Us Feedback</a>
                </p>
            </div>

            <!-- Grid column -->
            <hr class="w-100 clearfix d-md-none">

            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
                <h6 class="text-uppercase mb-4 ">Contact</h6>
                <p>
                    <i class="fa fa-home mr-3"></i> Nairobi, isHere, Kenya</p>
                <p>
                    <i class="fa fa-envelope mr-3"></i> info@gmail.com</p>
                <p>
                    <i class="fa fa-phone mr-3"></i> + 254 0 0 0 0</p>
                <p>
                    <i class="fa fa-print mr-3"></i> + 254 0 0 0 0</p>
            </div>
            <!-- Grid column -->

        </div>
        <!-- Footer links -->

        <hr>

        <!-- Grid row -->
        <div class="row d-flex align-items-center">

            <!-- Grid column -->
            <div class="col-md-7 col-lg-8">

                <!--Copyright-->
                <p class="text-center text-md-left">© 2020 Copyright:
                    <a href="/">
                        <strong> lowkeydigitals.com</strong>
                    </a>
                </p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-5 col-lg-4 ml-lg-0">

                <!-- Social buttons -->
                <div class="text-center text-md-right">
                    <ul class="list-unstyled list-inline">
                        <li class="list-inline-item">
                            <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

<!-- Footer -->
