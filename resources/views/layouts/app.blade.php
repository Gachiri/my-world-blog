<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <!-- Styles -->
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">



    <link href="http://netdna.bootstrapcdn.com/bootstrap/4.2.0/css/bootstrap.css" rel="stylesheet">
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>--}}
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="{{ asset('css/grayscale.css') }}" rel="stylesheet">


</head>
<body class="d-flex flex-column">
    <div id="page-container">

        @include('layouts.main_nav')

        <div id="content-wrap" class="mt-2">
                @yield('content')
        </div>
        <footer id="footer" class="text-light">
            @include('layouts.footer')
        </footer>
    </div>

</body>
</html>
