<div class="modal fade add-friends-modal" tabindex="-1" role="dialog"
     aria-labelledby="friends-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header ">
                <div class="container">
                    <div class="row">
                        <div class="col-6 offset-3">
                            <h5 class="modal-title text-center">Add new friend:</h5>
                        </div>
                        <div class="row mx-auto">
                            <form class="form-inline mt-4 mb-4" action="javascript:search();">
                                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"
                                       id="search-input">
                                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </form>
                        </div>

                    </div>
                </div>

                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div id="results"></div>
            </div>

        </div>
