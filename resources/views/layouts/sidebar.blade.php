


<div class="nav-side-menu">
    <div class="brand">
        <a class="navbar-brand hide-nav" href="{{ url('/') }}">
            <img src="{{asset('images/nav-logo.png')}}" alt="Lowkey Digital" class="img-fluid logo-image">
        </a></div>

        <div class="menu-list">

            <ul id="menu-content" class="menu-content collapse out">
                <li>
                  <a href="#">
                  <i class="fa fa-dashboard fa-lg"></i> Dashboard
                  </a>
                </li>

                <li  data-toggle="collapse" data-target="#products" class="collapsed">
                  <a href="#"><i class="fa fa-gift fa-lg"></i>  <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="products">
                    <li class="active"><a href="#">RFT-H1</a></li>
                    <li><a href="#">RFT-H2</a></li>
                    <li><a href="#">BTB-H1</a></li>
                    <li><a href="#">BTB-H2</a></li>
                </ul>


                <li data-toggle="collapse" data-target="#service" class="collapsed">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> Auswertungen <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="service">
                  <li>Trendmonitoring</li>
                  <li>Alarmmonitoring</li>
                  <li>Audit-Trail</li>
                </ul>


                <li data-toggle="collapse" data-target="#report" class="collapsed">
                  <a href="#"><i class="fa fa-car fa-lg"></i> Reporting <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="report">
                  <li>Alarmstatistik</li>
                  <li>Prozessfähigkeit</li>
                </ul>


                 <li>
                  <a href="#">
                  <i class="fa fa-user fa-lg"></i> Profile
                  </a>
                  </li>

                 <li data-toggle="collapse" data-target="#new" class="collapsed">
                  <a href="#"><i class="fa fa-car fa-lg"></i> Service <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="new">
                  <li>Sensorkonfiguration</li>
                  <li>Betriebsarten</li>
                </ul>

            </ul>
     </div>
</div>
