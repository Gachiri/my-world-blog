{{--<div class="rating rating2 disabled">--}}
{{--		<a href="#5" title="Give 5 stars">★</a>--}}
{{--		<a href="#4" title="Give 4 stars">★</a>--}}
{{--		<a href="#3" title="Give 3 stars">★</a>--}}
{{--		<a href="#2" title="Give 2 stars">★</a>--}}
{{--		<a href="#1" title="Give 1 star">★</a>--}}
{{--</div>--}}

<div class="row no-gutters">
    <div class="col-sm-2">
        <card>
            <img src="{{asset('images/small-logo.png')}}" alt="Logo" class="small-logo">
        </card>
    </div>


    <div class="col-sm-6">
                <div class="my-auto">
                    <h4>Mountains</h4>
                    @for ($i=1; $i <= 5 ; $i++)
                        <span class="glyphicon glyphicon-star{{ ($i <= 5) ? '' : '-empty'}}"></span>
                        <a href="#1" class="disabled">★</a>
                    @endfor
                    <p class="my-2">Opens at: 8:00 am  Closes at: 5:00 pm</p>
                </div>
    </div>

    <div class="col-sm-4">
        <p class="my-2">Address</p>

        <p>
            At this point there are only few things left to finish,
            store the user’s reviews somehow and also
            connect all the routes to the functionality that we have built.
        </p>
    </div>
</div>
<hr>
