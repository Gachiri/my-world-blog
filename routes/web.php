<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/','SearchController@index');
Route::get('/search','SearchController@search');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home', 'AdminController@index')->name('home');

Route::get('posts', 'HomeController@posts')->name('posts');

Route::post('posts', 'HomeController@postPost')->name('posts.post');

Route::get('posts/{id}', 'HomeController@show')->name('posts.show');

//Business Routes
Route::get('/for/business', function (){
    return view('business.for_business');
})->name('for_business');

Route::get('/customer/main', function (){
    return view('customer.customer_main');
})->name('customer_main');

Route::get('/business/details', function (){
    return view('business.details');
})->name('details');
